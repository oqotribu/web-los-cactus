$( document ).ready(function() {

    createEvents()

});

function createEvents(){

    //Boton 360-1
    $('.btn-360-1').click(function(){
        
        $('.vista-360-container > iframe').prop('src', $('.vista-360-container > iframe').data('src'))
        $('.vista-360-container').css('height',$('#jarallax-container-0').css('height'))
        $('.vista-360-container').offset($('main').offset())
        $('.vista-360-container').toggle( "scale" );
    })

     //Boton 360-2
    $('.btn-360-2').click(function(){
        
        $('.vista-360-container-2 > iframe').prop('src', $('.vista-360-container-2 > iframe').data('src'))
        $('.vista-360-container-2').css('height',$('#seccion-360-2').css('height'))
        $('.vista-360-container-2').offset($('#seccion-360-2').offset())
        $('.vista-360-container-2').toggle( "scale" );
    })

    //Boton cerrar 360-1
    $('.btn-close-1').click(function(){
        $('.vista-360-container').toggle( "scale" );
    })

    //Boton cerrar 360-2
    $('.btn-close-2').click(function(){
        $('.vista-360-container-2').toggle( "scale" );
    })

    //Eventos al hacer scroll

    $(function(){
        //Variables que controlan si la imagen se esta mostrando o no
        let bgBlanco02=false;
        let bgRojo02=false;
        let bgGris02=false;
        let bgNegro02=false;

        let bgBlanco01=true;
        let bgRojo01=true;
        let bgGris01=true;
        let bgNegro01=true;

        //detectar scroll hacia abajo
        var obj = $(document);         
        var obj_top = obj.scrollTop()  
        
        obj.scroll(function(){

            var obj_act_top = $(this).scrollTop();    
            if(obj_act_top > obj_top){
                //scroll hacia abajo
                
                if(!bgBlanco02 
                    && obj_act_top >= $('#bg-blanco-01').offset().top - 200
                    && obj_act_top <= ($('#bg-blanco-01').offset().top + $('#bg-blanco-01').height())){                       
                        $('#bg-blanco-02').css('background-image',$('#bg-blanco-02').data('imagen'))
                        $('#bg-blanco-02').show("slide", { direction: "right" }, 800);
                        bgBlanco02=true;
                        bgBlanco01=false;
                    }   
                if(!bgRojo02 
                    && obj_act_top >= $('#bg-rojo-01').offset().top - 200
                    && obj_act_top <= ($('#bg-rojo-01').offset().top + $('#bg-rojo-01').height())){                       
                        $('#bg-rojo-02').css('background-image',$('#bg-rojo-02').data('imagen'))
                        $('#bg-rojo-02').show("slide", { direction: "right" }, 800);
                        bgRojo02=true;
                        bgRojo01=false;
                    }     
                    if(!bgGris02 
                        && obj_act_top >= $('#bg-gris-01').offset().top - 200
                        && obj_act_top <= ($('#bg-gris-01').offset().top + $('#bg-gris-01').height())){                       
                            $('#bg-gris-02').css('background-image',$('#bg-gris-02').data('imagen'))
                            $('#bg-gris-02').show("slide", { direction: "right" }, 800);
                            bgGris02=true;
                            bgGris01=false;

                        }  
                    if(!bgNegro02 
                        && obj_act_top >= $('#bg-negro-01').offset().top - 200
                        && obj_act_top <= ($('#bg-negro-01').offset().top + $('#bg-negro-01').height())){                       
                            $('#bg-negro-02').css('background-image',$('#bg-negro-02').data('imagen'))
                            $('#bg-negro-02').show("slide", { direction: "right" }, 800);
                            bgNegro02=true;
                            bgNegro01=false;
                        }        
            }else{
                //scroll hacia arriba
                
                if(!bgNegro01 
                    && obj_act_top <= $('#bg-negro-02').offset().top + $('#bg-negro-02').height()-300
                    && obj_act_top > $('#bg-negro-02').offset().top ){                       
                        $('#bg-negro-01').show();
                        $('#bg-negro-02').fadeOut(2000);
                        bgnegro01=true;
                        bgNegro02=false;
                    } 
                if(!bgGris01 
                    && obj_act_top <= $('#bg-gris-02').offset().top + $('#bg-gris-02').height()-300
                    && obj_act_top > $('#bg-gris-02').offset().top ){                       
                        $('#bg-gris-01').show();
                        $('#bg-gris-02').fadeOut(2000);
                        bgGris01=true;
                        bgGris02=false;
                    }    
                if(!bgRojo01 
                    && obj_act_top <= $('#bg-rojo-02').offset().top + $('#bg-rojo-02').height()-300
                    && obj_act_top > $('#bg-rojo-02').offset().top ){     
                        $('#bg-rojo-01').show();
                        $('#bg-rojo-02').fadeOut(2000);
                        bgRojo01=true;
                        bgRojo02=false;
                    }        
                if(!bgBlanco01 
                    && obj_act_top <= $('#bg-blanco-02').offset().top + $('#bg-blanco-02').height()-300
                    && obj_act_top > $('#bg-blanco-02').offset().top ){                       
                        $('#bg-blanco-01').show();
                        $('#bg-blanco-02').fadeOut(2000);
                        bgBlanco01=true;
                        bgBlanco02=false;
                    }                       
            }
            obj_top = obj_act_top;                  //almacenar scroll top anterior
        });
    });
}